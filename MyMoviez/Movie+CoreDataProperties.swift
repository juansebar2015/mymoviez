//
//  Movie+CoreDataProperties.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/23/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Movie {

    @NSManaged var image: NSData?
    @NSManaged var title: String?
    @NSManaged var movieDescription: String?
    @NSManaged var imdbLink: String?
    @NSManaged var moviePlot: String?

}
