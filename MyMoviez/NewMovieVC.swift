//
//  NewMovieVC.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/23/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import CoreData

class NewMovieVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var movieTitle: UITextField!
    @IBOutlet weak var movieDescription: UITextField!
    @IBOutlet weak var movieURL: UITextField!
    @IBOutlet weak var moviePlot: UITextField!
    @IBOutlet weak var movieImage: UIImageView!
    
    var imagePicker : UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        movieImage.layer.cornerRadius = 8.0
        movieImage.clipsToBounds = true
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        dismissViewControllerAnimated(true, completion: nil)
        movieImage.image = image
        
    }
    
    @IBAction func addImage(sender: AnyObject) {
        
        presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func createPost(sender: AnyObject) {
        
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        
        let entity = NSEntityDescription.entityForName("Movie", inManagedObjectContext: context)!
        
        let movie = Movie(entity: entity, insertIntoManagedObjectContext: context)
        
        movie.setImg(movieImage.image!)
        movie.title = movieTitle.text
        movie.movieDescription = movieDescription.text
        movie.imdbLink = movieURL.text
        movie.moviePlot = moviePlot.text
        
        
        context.insertObject(movie)
        
        do {
            try context.save()
        } catch {
            print ("Could not save movie!")
        }
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func onBackPressed (sender: AnyObject!) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
