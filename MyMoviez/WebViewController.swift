//
//  WebViewController.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/24/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var container: UIView!
    var webView: WKWebView!
    var URL: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView = WKWebView()
        container.addSubview(webView)
    }

    override func viewDidAppear(animated: Bool) {
        
        let frame = CGRectMake(0, 0, container.bounds.width, container.bounds.height)
        webView.frame = frame
        
        loadRequest()
        
    }
    
    func loadRequest() {
        
        let url = NSURL(string: URL)!
        let request = NSURLRequest(URL: url)
        
        webView.loadRequest(request)
        
    }
    
    @IBAction func onBackPressed (sender: AnyObject!) {
        
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    

}
