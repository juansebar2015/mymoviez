//
//  MovieCell.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    var ImdbURL : String!
    //@IBOutlet weak var label: UILabel!
    @IBOutlet weak var imdbButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.movieImage.layer.cornerRadius = 8.0
        self.movieImage.clipsToBounds = true
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(movie: Movie){
        self.movieImage.image = movie.getImage()
        self.movieTitle.text = movie.title
        self.movieDescription.text = movie.movieDescription
        self.ImdbURL = movie.imdbLink
        
        //label.userInteractionEnabled = true
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("labelPressed"))
//        label.addGestureRecognizer(gestureRecognizer)
    }
    
    @IBAction func goToImdb (sender: AnyObject!) {
        
        //Go to http://www.imdb.com
        
        
    }
    
//    func labelPressed(){
//        print("Label pressed")
//        //Your awesome code.
//    }
}
