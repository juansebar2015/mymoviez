//
//  MovieDetailVC.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/23/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController {

    @IBOutlet weak var movieImg: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var moviePlot: UILabel!
    var imdbURL : String!
    var movie : Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.movieTitle.text = movie.title
        self.movieDescription.text = movie.movieDescription
        self.moviePlot.text = movie.moviePlot
        self.movieImg.image = movie.getImage()
        self.imdbURL = movie.imdbLink
    }
    

    @IBAction func imdbLink(sender: AnyObject) {
        
        //Open Web site NSURLRequest
        performSegueWithIdentifier("goToWebVC", sender: nil)
        
    }

    @IBAction func onBackPressed(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "goToWebVC" {
            
            if let webVC = segue.destinationViewController as? WebViewController {
                
                webVC.URL = imdbURL
            }
        }
    }
}
