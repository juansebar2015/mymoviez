//
//  ViewController.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var movies = [Movie]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }

    func labelPressed() {
        print("inside the function")
    }
    
    //Reload new movies
    override func viewWillAppear(animated: Bool) {
        
        fetchAndSetResults()
        tableView.reloadData()
        
    }
    
    func fetchAndSetResults () {
        
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Movie")
        
        do {
            let results = try context.executeFetchRequest(fetchRequest)
            self.movies = results as! [Movie]
            
        } catch {
            print("Something went wrong")
        }

    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //When a cell is selected go to the details view
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let movie = movies[indexPath.row]
        
        performSegueWithIdentifier("goToMovieDetailVC", sender: movie)
        
    }
    
    // Populate the cells
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("movieCell") as? MovieCell {
            
            let movie = movies[indexPath.row]
            
            cell.configureCell(movie)
            cell.imdbButton.tag = indexPath.row
            
            return cell
            
        } else {
            return MovieCell()
        }
        
    }
    
    // Edit the contents of the table
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        /*
         * Steps to delete a cell
         *   1. Check the behavior
         *   2. Delete item from data source (CoreData)
         *       - Get the context
         *       - Call context.deleteObject(array[indexPath.row] as NSManagedObject)
         *       - Save the changes (context.save())
         *   3. Call tableView.deleteRowsAtIndexPath()
         */
        
        // Implements the swipe to delete functionality
        if editingStyle == .Delete {
            
            let app = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = app.managedObjectContext
            
            // Delete the particular cell from the context
            context.deleteObject(movies[indexPath.row] as NSManagedObject)
            
            // Save the changes made to Core Data
            do {
               try context.save()
            } catch {
                print("Couldn't delete movie!")
            }
            
            //Remove the reference from the local
            movies.removeAtIndex(indexPath.row)
            
            // Delete the row from the table
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        
    }
    
    @IBAction func onImdbPressed (sender: AnyObject!) {
        
        if let button = sender as? UIButton {
            
            let movie = movies[button.tag]
            
            performSegueWithIdentifier("goStraightToWebVC", sender: movie)
        }
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "goToMovieDetailVC" {
            
            if let movieDetail = segue.destinationViewController as? MovieDetailVC {
                
                if let movie = sender as? Movie {
                    movieDetail.movie = movie
                    
                }
            }
        } else if segue.identifier == "goStraightToWebVC" {
            if let webVC = segue.destinationViewController as? WebViewController {
                
                if let movie = sender as? Movie {
                    webVC.URL = movie.imdbLink
                }
                
            }
        }
        
        
    }
    
    
    
}

