//
//  Movie.swift
//  MyMoviez
//
//  Created by Juan Ramirez on 5/22/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class Movie: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func setImg (img: UIImage!) {
        
        let data = UIImagePNGRepresentation(img)
        
        self.image = data
        
    }
    
    func getImage() -> UIImage {
        
        let image = UIImage(data: self.image!)!
        
        return image
    }
    
}
